<?php

require_once  __DIR__ . '/vendor/autoload.php';

use src\entities\HomeDuck;
use src\entities\MallardDuck;
use src\entities\ModelDuck;
use src\entities\RedHeadDuck;
use src\entities\RubberDuck;

$homeDuck = new HomeDuck();
print_r('Name: '.$homeDuck->display() . ' Can: ' . $homeDuck->speak() . ' and ' . $homeDuck->swim());

$mallardDuck = new MallardDuck();
print_r('Name: '.$mallardDuck->display() . ' Can: ' . $mallardDuck->speak() . ' and ' . $mallardDuck->swim() . ' and ' . $mallardDuck->fly());

$modelDuck = new ModelDuck();
print_r('Name: '.$modelDuck->display() . ' Can: '  .  $modelDuck->swim());

$rubberDuck = new RubberDuck();
print_r('Name: '.$rubberDuck->display() . ' Can: '  .  $rubberDuck->swim() . 'and ' . $rubberDuck->speak());

$redHeadDuck = new RedHeadDuck();
print_r('Name: '.$redHeadDuck->display() . ' Can: '  .  $redHeadDuck->swim() . 'and ' . $redHeadDuck->speak() . ' and ' . $redHeadDuck->fly());
