<?php
namespace src\Interfaces;

interface SpeakInterface
{
    public function speak();
}