<?php

namespace src\Interfaces;

interface FlyInterface
{
    public function fly();
}