<?php
namespace src\entities;

use src\Interfaces\FlyInterface;
use src\Interfaces\SpeakInterface;

class RedHeadDuck extends DuckAbstract implements FlyInterface, SpeakInterface
{

    const NAME = 'read head duck';
    /**
     * @return mixed
     */
    function display()
    {
        return self::NAME;
    }

    /**
     * @return mixed
     */
    public function fly()
    {
        return 'fly';
    }

    /**
     * @return mixed
     */
    public function speak()
    {
        return 'quack';
    }
}
