<?php

namespace src\entities;

class ModelDuck extends DuckAbstract
{
    const NAME = 'model duck';
    /**
     * @return mixed
     */
    function display()
    {
        return self::NAME;
    }

}
