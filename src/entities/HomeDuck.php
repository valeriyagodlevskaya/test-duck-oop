<?php

namespace src\entities;

use src\Interfaces\SpeakInterface;

class HomeDuck extends DuckAbstract implements SpeakInterface
{

    const NAME = 'Home duck';

    public function display()
    {
        return self::NAME;
    }


    /**
     * @return mixed
     */
    public function speak()
    {
        return 'quack';
    }
}