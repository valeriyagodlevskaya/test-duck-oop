<?php
namespace src\entities;

abstract class DuckAbstract
{
    abstract function display();

    public function swim()
    {
        return 'swim';
    }

}