<?php
namespace src\entities;

use src\Interfaces\SpeakInterface;

class RubberDuck extends DuckAbstract implements SpeakInterface
{
    const NAME = 'rubber duck';

    /**
     * @return mixed
     */
    function display()
    {
        return self::NAME;
    }

    /**
     * @return mixed
     */
    public function speak()
    {
        return 'piiiiiiiiiiii';
    }
}
